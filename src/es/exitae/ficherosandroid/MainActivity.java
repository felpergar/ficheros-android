package es.exitae.ficherosandroid;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	private EditText editTexto;
	private Button btnLeer, btnGuardar;

	private String nombreFichero = Environment.getExternalStorageDirectory().getAbsolutePath() + "/pruebas.txt";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		editTexto = (EditText) findViewById(R.id.editTexto);

		btnLeer = (Button) findViewById(R.id.btnLeer);
		btnLeer.setOnClickListener(this);

		btnGuardar = (Button) findViewById(R.id.btnGuardar);
		btnGuardar.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnLeer:

			try {
				editTexto.setText("");

				InputStream entrada = new FileInputStream(nombreFichero);
				DataInputStream dataEntrada = new DataInputStream(entrada);

				String line;
				while ((line = dataEntrada.readLine()) != null) {
					editTexto.setText(editTexto.getText().toString() + line + "\n");
				}

				dataEntrada.close();
				entrada.close();

			} catch (FileNotFoundException e) {
				editTexto.setText("");
				Toast.makeText(this, "No se localiza el fichero", Toast.LENGTH_SHORT).show();

			} catch (IOException e) {
				Toast.makeText(this, "Ha ocurrido un error de lectura", Toast.LENGTH_SHORT).show();
			}

			break;

		case R.id.btnGuardar:

			try {
				OutputStream salida = new FileOutputStream(nombreFichero);
				DataOutputStream dataSalida = new DataOutputStream(salida);

				String texto = editTexto.getText().toString();
				dataSalida.writeChars(texto);

				dataSalida.flush();
				dataSalida.close();
				salida.close();

			} catch (FileNotFoundException e) {

			} catch (IOException e) {
				Toast.makeText(this, "Ha ocurrido un error de escritura", Toast.LENGTH_SHORT).show();
			}

			break;

		}
	}

}
